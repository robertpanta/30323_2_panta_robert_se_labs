package Airline;

public class NonMember extends Passenger {

    public NonMember(int a, String n) {
        super(a, n); //call of the parent constructor (passenger); age; name
    }

    @Override
    public double applyDiscount(double p) { //apply discount at the price of the ticket, for the non-members
        if (age > 65) { //if the non-members are less than 65 years old, they have a discount of 10% of the full price of the ticket
            p = (p * 9) / 10;
            return p;
        }
        return p;
    }
}