package Airline;

public abstract class Passenger {

    String name;
    int age;

    public Passenger(int a, String n) {
        age = a;
        name = n;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    abstract public double applyDiscount(double p);
}