package Airline;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Manager {

    List<Flight> flights; //list fot the flights
    List<Ticket> tickets; //list for the tickets

    public Manager() {
        flights = new ArrayList<Flight>();
        tickets = new ArrayList<Ticket>();
    }

    public void createFlights() {
        int FN, c; //flight number; capacity
        double op; //original price
        String origin, destination, dt; //origin; destination; departure time

        Scanner scan1 = new Scanner(System.in); //soon scan for int
        Scanner scan2 = new Scanner(System.in); //soon scan for line
        Scanner scan3 = new Scanner(System.in); //soon scan for double

        System.out.println("Enter flight number:"); //print " ... "
        FN = scan1.nextInt(); //scan for int

        System.out.println("Enter flight origin:");
        origin = scan2.nextLine(); // scan for line

        System.out.println("Enter flight destination:");
        destination = scan2.nextLine(); //scan for line

        System.out.println("Enter flight departure time and date:");
        dt = scan2.nextLine(); //scan for line

        System.out.println("Enter flight capacity:");
        c = scan1.nextInt(); //scan for int

        System.out.println("Enter original price of ticket for LOW cost:");
        op = scan3.nextDouble(); //scan for double

        Flight F = new Flight(FN, origin, destination, dt, c, op); //new Flight will contain (flight number; origin; destination; departure time; capacity and original price)
        flights.add(F); //the new Flight will be added to flights array list
        System.out.println("The following flight has been created:");
        System.out.println(F); //the new flight will be printed
    }

    public void displayAvailableFlights(String origin, String destination) { //displayAvailableFlights method
        int size, seats;
        boolean flag = true;
        String o, d; //origin; destination
        size = flights.size(); //size= size of the flights array list

        for (int i = 0; i < size; i++) {
            o = flights.get(i).getOrigin();
            d = flights.get(i).getDestination();
            seats = flights.get(i).getNumberOfSeatsLeft();

            if ((o.equals(origin) == true) && (d.equals(destination) == true) && seats > 0) {
                //if the wanted origin and destination are available for the same flight, the available flights will show, if not, then there won't be any available flights
                if (flag) {
                    System.out.println("List of available flights:");
                    flag = false;
                }
                System.out.println(flights.get(i));
            }
        }
        if (flag) {
            System.out.println("No available flights");
        }
    }

    public Flight getFlight(int flightNumber) { //getFlight method
        int size = flights.size(); //size= size of the flights array list

        for (int i = 0; i < size; i++) {
            //if the wanted flight number exists, the specific flight number will show
            if (flights.get(i).getFlightNumber() == flightNumber) {
                return flights.get(i);
            }
        }
        return null;
    }

    public void bookSeat(int flightNumber, Passenger p) { //bookSeat method
        int size = flights.size(); //size= size of the flights array list
        boolean flag = false;
        int index = 0, seats;

        for (int i = 0; i < size; i++) {
            if (flights.get(i).getFlightNumber() == flightNumber) { //the size of the seats will be encountered
                flag = true;
                index = i; //index= the number of seats in a plane
                break;
            }
        }

        double price;
        seats = flights.get(index).getNumberOfSeatsLeft(); //seats= places left in the plane

        if (flag == true && (seats > 0)) { //if there are any free seats, a seat can be booked
            flights.get(index).bookASeat();
            price = p.applyDiscount(flights.get(index).getPrice());
            //the specific discount will be applied for the ticket, depending on age, years of membership
            Ticket a = new Ticket(flights.get(index), p, price); //the new ticket will contain all the information needed and updated
            tickets.add(a); //the new Ticket will be added to tickets array list
            System.out.println("You have successfully booked a seat for flight number " + flightNumber + "!");
            System.out.println("Ticket: " + a + ".");

        } else {
            if (flag == true && seats == 0) { //if the number of free seats is 0, it's impossible to have any booked tickets left
                System.out.println("The flight " + flightNumber + " is full you can't book a ticket for this flight.");

            } else if (!flag) { //if there isn't any wanted flight number, the flight does not exist.
                System.out.println("The flight " + flightNumber + " does not exist.");
            }
        }
    }

    public static void main(String[] args) {
        Manager M = new Manager();
        Flight F;
        Passenger P;
        Scanner scan1 = new Scanner(System.in);
        Scanner scan2 = new Scanner(System.in);
        String input, destination, origin, name;
        int FN, age, years;
        boolean flag = false;

        while (!flag) {
            System.out.println("Enter 1 if you would like to create a flight"); //createFlights method
            System.out.println("Enter 2 if you would like to display all flights"); //displayAvailableFlights method
            System.out.println("Enter 3 if you would like to get information on a flight"); //getFlight method
            System.out.println("Enter 4 if you would like to book a seat"); //bookSeat method
            System.out.println("Enter 5 if you would like to terminate the program"); //Exit the program
            input = scan1.nextLine();
            switch (input) {
                case "1":
                    M.createFlights(); //the manager creates the information of the flights available
                    break;
                case "2":
                    System.out.println("Enter flight origin:");
                    origin = scan1.nextLine();
                    System.out.println("Enter flight destination:");
                    destination = scan1.nextLine();
                    M.displayAvailableFlights(origin, destination); //the manager displays the available flights taking the origin and destination into consideration
                    break;
                case "3":
                    System.out.println("Enter flight number:");
                    FN = scan2.nextInt(); //the flight number given by the passenger
                    F = M.getFlight(FN); //the wanted flight with the given flight number
                    if (F == null) { //if the wanted number flight will be equal with null, it means it doesn't exist
                        System.out.println("Flight " + FN + " does not exist.");
                    } else {
                        System.out.println("Information for flight " + FN + ":");
                        System.out.println(F); //the flight with the given flight number
                    }
                    break;
                case "4":
                    System.out.println("If passenger is non-member enter n, if passenger is a member enter m:");
                    input = scan1.nextLine(); //n or m
                    System.out.println("Enter passengers age:");
                    age = scan2.nextInt();
                    System.out.println("Enter passengers name:");
                    name = scan1.nextLine();
                    System.out.println("Enter flight number:");
                    FN = scan2.nextInt();

                    if (input.equals("n")) { //->non-member
                        P = new NonMember(age, name); // Passenger=NonMember with specific age and name
                        M.bookSeat(FN, P); //the booked seat will have a unique flight number and passenger
                    } else if (input.equals("m")) { //->member
                        System.out.println("For how many years has the passenger been a member?");
                        years = scan2.nextInt();
                        P = new Member(years, age, name);
                        M.bookSeat(FN, P); //the booked seat will have a unique flight number and passenger
                        //depending on the years of membership, the passenger will have a different price for the ticket
                    }
                    break;
                case "5":
                    flag = true;
                    System.out.println("Program is terminated!"); //exit the program
                    break;
                default:
                    System.out.println("You entered an invalid option!"); //in case the passenger inserts something else than "1, 2, 3, 4, 5"
                    break;
            }
        }
    }
}