package Airline;

public class Ticket {

    private Flight flight;
    private Passenger passenger;
    private double price;
    private int number;
    private static int count = 0;

    public Ticket(Flight f, Passenger p, double pr) {
        count++; //count for the ticket number
        flight = f;
        passenger = p;
        price = pr;
        number = count;
    }

    @Override
    public String toString() {
        String one = passenger.getName() + ", Flight " + flight.getFlightNumber();
        String two = ", " + flight.getOrigin() + " to " + flight.getDestination();
        String three = ", " + flight.getDepartureTime();
        String four = ", original price: " + flight.getPrice() + "€";
        String five = ", ticket price: " + price + "€" + ", ";
        Level var = Level.LOW;
        return one + two + three + four + five + var + " Cost\n"; //final information about the booked ticket
    }

    public Flight getFlight() {
        return flight;
    }

    public void setFlight(Flight f) {
        flight = f;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger p) { passenger = p; }

    public double getPrice() {
        return price;
    }

    public void setPrice(double pr) {
        price = pr;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int n) {
        number = n;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int n) {
        count = n;
    }

}