package Airline;

public class Flight {

    private int flightNumber;
    private String origin;
    private String destination;
    private String departureTime;
    private int capacity;
    private int numberOfSeatsLeft;
    private double originalPrice;

    public Flight(int f, String o, String d, String dp, int c, double op) {
        if (d.equals(o)) { //if the destination and the origin are the same=>" ... "
            throw new IllegalArgumentException("Destination and origin are the same!");
        }
        flightNumber = f;
        origin = o;
        destination = d;
        departureTime = dp;
        capacity = c;
        numberOfSeatsLeft = c;
        originalPrice = op;
    }

    public boolean bookASeat() {
        if (numberOfSeatsLeft > 0) { //when booking a seat, if the number of free seats is more than 0, the new number of left seats will be the precedent-1
            numberOfSeatsLeft = numberOfSeatsLeft - 1;
            return true;

        }
        return false;
    }

    public double getPrice() {

        return originalPrice;
    }

    public String getOrigin() {

        return origin;
    }

    public String getDestination() {

        return destination;
    }

    public String getDepartureTime() {

        return departureTime;
    }

    public int getFlightNumber() {

        return flightNumber;
    }

    public int getNumberOfSeatsLeft() {

        return numberOfSeatsLeft;
    }

    public int getCapacity() {

        return capacity;
    }

    public void setPrice(double p) {

        originalPrice = p;
    }

    public void setOrigin(String o) {

        origin = o;
    }

    public void setDestination(String d) {

        destination = d;
    }

    public void setDepartureTime(String dp) {

        departureTime = dp;
    }

    public void setFlightNumber(int f) {

        flightNumber = f;
    }

    public void setNumberOfSeatsLeft(int c) {

        numberOfSeatsLeft = c;
    }

    public void setCapacity(int c) {

        capacity = c;
    }

    @Override
    public String toString() {
        String one = "Flight " + flightNumber;
        String two = ", " + origin + " to " + destination;
        String three = ", " + departureTime;
        String four = ", original price: " + originalPrice + "€" + ", ";
        Level var = Level.LOW;
        return one + two + three + four + var + " Cost\n"; //information about the booked ticket
    }
}