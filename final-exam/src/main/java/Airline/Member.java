package Airline;

public class Member extends Passenger {

    private int yearsOfMembership;

    public Member(int yom, int a, String n) {
        super(a, n); //call of the parent constructor (passenger); age; name
        yearsOfMembership = yom;
    }

    @Override
    public double applyDiscount(double p) { //apply discount at the price of the ticket, for the members
        if (yearsOfMembership > 5) {
            //if the member has more than 5 years of membership, he will pay the half price of the full ticket price
            p = p / 2;
            return p;
        } else if (yearsOfMembership <= 5 && yearsOfMembership > 1) {
            //if the member has less than 5 and more than one year of membership, he will pay 90% of the full ticket price
            p = (p * 9) / 10;
            return p;
        }
        return p;
    }
}
