package robert.panta.lab2.ex6;

import java.util.Scanner;


public class Ex6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Insert a number for calculating its factorial: ");
        int n = scanner.nextInt();
        int i;
        int factor = 1;

        for (i = 1; i <= n; i++) {
            factor = factor * i;
        }
        if (i == 0)
            factor = 1;
        System.out.println("Factorial of " + n + " is " + factor);
    }
}

/*
public class Ex6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Insert a number for calculating its factorial: ");
        int n = scanner.nextInt();

        int factor = 1;
        factor = factorial(n);
        System.out.println("Factorial of " + n + " is " + factor);
    }

    static int factorial(int f) {
        if (f == 0)
            return 1;
        else
            return (f * factorial(f - 1));
    }
}
*/