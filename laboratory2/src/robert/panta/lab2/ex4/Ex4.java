package robert.panta.lab2.ex4;

import java.util.Scanner;

public class Ex4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("The number of elements for the vector:");
        int n = scanner.nextInt();
        int[] array = new int[n];

        System.out.println("The elements of the array:");
        int max = 0;
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }
        for (int i = 0; i < n; i++) {
            if (max < array[i]) max = array[i];
        }
        System.out.println(max);
    }
}