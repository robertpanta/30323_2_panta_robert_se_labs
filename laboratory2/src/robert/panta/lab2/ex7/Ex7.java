package robert.panta.lab2.ex7;

import java.util.Random;
import java.util.Scanner;

public class Ex7 {
    public static void main(String[] args) {
        Random randomNumber = new Random();
        int max = 10;
        int min = 1;
        int number = randomNumber.nextInt((max - min) + 1) - min;
        System.out.println("Guess the number! \nWrite it down:");

        int i = 0;
        for (i = 0; i < 5; i++) {
            Scanner scanner = new Scanner(System.in);
            int n = scanner.nextInt();
            if (n == number) {
                System.out.println("You guessed the number!");
                break;
            } else if (number < n) {
                System.out.println("Wrong answer, your number it too high");
            } else if (number > n) {
                System.out.println("Wrong answer, your number is too low");
            }
            i++;
            if (i == 5) {
                System.out.println("\nYou lost");
            }
        }
    }
}