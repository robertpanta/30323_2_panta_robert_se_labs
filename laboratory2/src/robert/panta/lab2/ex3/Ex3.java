package robert.panta.lab2.ex3;

import java.util.Scanner;

public class Ex3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Please insert a number: ");
        int a = scanner.nextInt();

        System.out.println("Please insert a second number: ");
        int b = scanner.nextInt();

        int prime = 0;

        while (a < b) {
            if (primeOrNot(a)) {
                System.out.print(a + " is a prime number\n");
                prime++;
            }
            ++a;
        }
        System.out.println("there are " + prime + " prime numbers\n");

    }

    public static boolean primeOrNot(int m) {
        boolean good = true;
        if (m <= 1) {
            return false;
        }
        for (int i = 2; i <= m / 2; i++) { //number to be checked => m
            if (m % i == 0) {
                good = false;
                System.out.println(m + " is not a prime number");
                break;
            }
        }
        return good;
    }
}