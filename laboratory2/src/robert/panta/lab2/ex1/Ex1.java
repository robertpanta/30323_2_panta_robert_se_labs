package robert.panta.lab2.ex1;

import java.util.Scanner;

public class Ex1 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Enter two numbers: \n");
        int x = input.nextInt();
        int y = input.nextInt();
        if (x < y)
            System.out.println(y);
        else
            System.out.println(x);
    }
}
