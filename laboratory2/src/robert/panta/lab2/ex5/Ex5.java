package robert.panta.lab2.ex5;

import java.util.Arrays;

public class Ex5 {
    public static void main(String[] args) {
        double vect[] = new double[10];
        System.out.println("The 10 randomly generated numbers: ");
        for (int i = 0; i < vect.length; i++) {
            vect[i] = Math.random();
            System.out.println(vect[i]);
        }
        System.out.println("The result using BubbleSort Method is: " + Arrays.toString(BubbleSortMethod(vect)));
    }

    private static double[] BubbleSortMethod(double[] vect) {
        for (int i = 0; i < vect.length - 1; i++) {
            for (int j = 1; j < vect.length - i; j++) {
                if (vect[j - 1] > vect[j]) {
                    double aux = vect[j - 1];
                    vect[j - 1] = vect[j];
                    vect[j] = aux;
                }
            }
        }
        return vect;
    }
}