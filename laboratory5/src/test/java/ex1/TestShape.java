package ex1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestShape {

    @Test
    public void testShapes() {
        Shape[] shapes = new Shape[50];
        shapes[0] = new Circle(2.0);
        shapes[1] = new Circle(4.0, "blue", true);
        shapes[2] = new Rectangle(2.0, 5.0);
        shapes[3] = new Rectangle(3.0, 6.0, "pink", false);
        shapes[4] = new Square(2.0);
        shapes[5] = new Square(5.0, "blue", false);

        assertEquals(12.566370614359172, shapes[0].getPerimeter());
        assertEquals(25.132741228718345, shapes[1].getPerimeter());
        assertEquals(14.0, shapes[2].getPerimeter());
        assertEquals(18.0, shapes[3].getPerimeter());
        assertEquals(8.0, shapes[4].getPerimeter());
        assertEquals(20.0, shapes[5].getPerimeter());
        assertEquals(12.566370614359172, shapes[0].getArea());
        assertEquals(50.26548245743669, shapes[1].getArea());
        assertEquals(10.0, shapes[2].getArea());
        assertEquals(18.0, shapes[3].getArea());
        assertEquals(4.0, shapes[4].getArea());
        assertEquals(25.0, shapes[5].getArea());

    }

}
