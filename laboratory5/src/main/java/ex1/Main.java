package ex1;

public class Main {
    public static void main(String[] args) {
        Shape[] shapes = new Shape[50];
        shapes[0] = new Circle(2.0);
        shapes[1] = new Circle(4.0, "blue", true);
        shapes[2] = new Rectangle(2.0, 5.0);
        shapes[3] = new Rectangle(3.0, 6.0, "pink", false);
        shapes[4] = new Square(2.0);
        shapes[5] = new Square(5.0, "blue", false);

        for (int i = 0; i < 5; i++) {
            System.out.println(shapes[i].toString() + " has the area of " + shapes[i].getArea() + "and the perimeter " + shapes[i].getPerimeter());

        }
    }
}
