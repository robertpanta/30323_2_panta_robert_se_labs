package ex4;


import java.util.Random;

public class LightSensor extends Sensor {
    Random rand = new Random();

    @Override
    public int readValue() {
        return rand.nextInt(100);
    }
}
