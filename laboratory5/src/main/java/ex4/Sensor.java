package ex4;

abstract class Sensor {
    private String location;

    public abstract int readValue();

    public String getLocation() {
        return location;
    }
}