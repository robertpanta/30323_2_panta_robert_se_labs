package ex3;


import java.util.Date;

public class Controller {
    public TemperatureSensor tempSensor = new TemperatureSensor();
    public LightSensor lightSensor = new LightSensor();

    public void cotrol() {
        for (int i = 0; i < 20; i++) {
            try {
                Thread.sleep(999);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println("The date was read on " + new Date());
            System.out.println("The room temperature is " + tempSensor.readValue() + " degrees");
            System.out.println("The light level is " + lightSensor.readValue() + '\n');
        }
    }
}
