package ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor {
    Random rand = new Random();

    @Override
    public int readValue() {

        return rand.nextInt(100);
    }
}