package ex2;

public class Main {
    public static void main(String[] args) {

        Image rl = new RealImage("Real Image");
        rl.display();

        Image ro = new RealImage("Rotate Image");
        ro.display();

        Image roImage = new ProxyImage("Proxy Image", ImageType.ROTATED);
        roImage.display();

        Image rImage = new ProxyImage("Proxy New", ImageType.REAL);
        rImage.display();
    }
}
