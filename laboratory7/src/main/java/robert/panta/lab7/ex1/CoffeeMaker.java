package robert.panta.lab7.ex1;

public class CoffeeMaker {
    static int coffeeCount = 0;
    int LIMIT = 5;

    public Coffee makeCoffee()
            throws MakeException {
        if (coffeeCount >= LIMIT) {
            throw (new MakeException("To much coffee made"));
        }
        System.out.println("Make a coffe");
        coffeeCount++;
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        Coffee coffee = new Coffee(t, c);
        return coffee;
    }


}
