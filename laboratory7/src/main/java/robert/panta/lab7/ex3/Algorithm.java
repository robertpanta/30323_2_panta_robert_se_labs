package robert.panta.lab7.ex3;

import java.io.*;

public class Algorithm {
    public static void main(String args[])
            throws IOException {

        String string;

        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Choose (enc/dec):");
        string = stdin.readLine();

        if (string.equals("enc")) {
            BufferedReader in = new BufferedReader(new FileReader("D:\\facult\\an2\\Sem 2\\Software engineering-Miron\\lab\\lab7\\E.enc"));
            BufferedWriter bw = null;
            FileWriter fw = new FileWriter("D:\\facult\\an2\\Sem 2\\Software engineering-Miron\\lab\\lab7\\D.dec");
            bw = new BufferedWriter(fw);

            String aux = new String();
            while ((aux = in.readLine()) != null) {
                char lin[] = aux.toCharArray();
                for (int i = 0; i < lin.length; i++) {
                    int ascii = (int) lin[i];
                    ascii = ascii << 1;
                    char convert = (char) ascii;
                    bw.write(convert);
                }
            }
            if (in != null)
                in.close();
            if (bw != null)
                bw.close();
        } else if (string.equals("dec")) {
            BufferedReader in = new BufferedReader(new FileReader("D:\\facult\\an2\\Sem 2\\Software engineering-Miron\\lab\\lab7\\D.dec"));
            BufferedWriter bw = null;
            FileWriter fw = new FileWriter("D:\\facult\\an2\\Sem 2\\Software engineering-Miron\\lab\\lab7\\E.enc");
            bw = new BufferedWriter(fw);

            String aux = new String();
            while ((aux = in.readLine()) != null) {
                char lin[] = aux.toCharArray();
                for (int i = 0; i < lin.length; i++) {
                    int ascii = (int) lin[i];
                    ascii = ascii >> 1;
                    char convert = (char) ascii;
                    bw.write(convert);
                }
            }
            if (in != null)
                in.close();
            if (bw != null)
                bw.close();
        } else {
            System.out.print("You didn't choose between enc and dec!");
        }
    }


}
