package robert.panta.lab7.ex4;

import java.io.*;

public class Main {

    public static void main(String args[]) {
        Car c = new Car("model21", 1000);
        try {
            FileOutputStream fileOut = new FileOutputStream("D:\\facult\\an2\\Sem 2\\Software engineering-Miron\\lab\\lab7\\Ex4.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(c);
            out.close();
            fileOut.close();
            System.out.printf("Serialized data is saved.");
        } catch (IOException i) {
            i.printStackTrace();
        }
        Car c1 = null;
        try {
            FileInputStream fileIn = new FileInputStream("D:\\facult\\an2\\Sem 2\\Software engineering-Miron\\lab\\lab7\\Ex4.ser");
            ObjectInputStream in = new ObjectInputStream(fileIn);
            c1 = (Car) in.readObject();
            in.close();
            fileIn.close();
        } catch (IOException i) {
            i.printStackTrace();
            return;
        } catch (ClassNotFoundException ce) {
            System.out.println("Employee class not found.");
            ce.printStackTrace();
            return;
        }

        System.out.println("\nDeserialized Employee.");
        System.out.println("Name: " + c1.getModel());
        System.out.println("Address: " + c1.getPrice());

    }

}