package robert.panta.lab7.ex2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CharacterCounter {
    public static void main(String args[])
            throws IOException {

        char charact;
        BufferedReader stdin = new BufferedReader(
                new InputStreamReader(System.in));
        System.out.println("The wanted character:");
        charact = stdin.readLine().charAt(0);

        int nr = 0;

        //citire din fisier
        BufferedReader in = new BufferedReader(
                new FileReader("D:\\facult\\an2\\Sem 2\\Software engineering-Miron\\lab\\lab7\\Test.txt"));
        String string = new String();
        while ((string = in.readLine()) != null) {
            char lin[] = string.toCharArray();
            for (int i = 0; i < lin.length; i++) {
                if (lin[i] == charact) {
                    nr++;
                }
            }
        }
        in.close();
        System.out.println("The character " + charact + " appears " + nr + " times in the file.");
    }


}
