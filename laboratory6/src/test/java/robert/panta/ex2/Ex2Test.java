package robert.panta.ex2;

import org.junit.jupiter.api.Test;
import robert.panta.lab6.ex2.Bank;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class Ex2Test {
    @Test
    public void testGetAccount() {
        Bank bank2 = new Bank();
        bank2.addAccount("Jack", 200);
        bank2.addAccount("Mary", 350);
        bank2.addAccount("Johnny", 100);
        bank2.addAccount("Robert", 160);
        bank2.addAccount("Joe", 440);
        System.out.println("\n The accounts sorted by name: ");
        bank2.printAccounts();
        assertEquals("\n The accounts sorted by name: " + bank2.getAllAccounts(), bank2.getAllAccounts());
    }
}
