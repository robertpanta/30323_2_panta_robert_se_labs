package robert.panta.lab6.ex3;

public class Main extends Bank {
    public static void main(String[] args) {

        Bank bank1 = new Bank();
        bank1.addAccount2("Jack", 200);
        bank1.addAccount2("Mary", 350);
        bank1.addAccount2("Johnny", 100);
        bank1.addAccount2("Robert", 160);
        bank1.addAccount2("Joe", 440);
        System.out.println("\n The accounts sorted by name: ");
        bank1.printAllAccounts();
    }
}
