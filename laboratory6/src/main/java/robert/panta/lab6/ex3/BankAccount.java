package robert.panta.lab6.ex3;

import java.util.Objects;

public class BankAccount implements Comparable {
    private String owner;
    private double balance;

    BankAccount(String owner, double balance) {
        this.balance = balance;
        this.owner = owner;
    }


    public void withdraw(double amount) {
        balance = balance - amount;
    }

    public void deposit(double amount) {
        balance = balance + amount;
    }

    public double getBalance() {

        return balance;
    }

    public String getOwner() {

        return owner;
    }

    public void setBalance(double balance) {

        this.balance = balance;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    @Override
    public String toString() {
        return "BankAccount: " +
                "owner='" + owner + '\'' +
                ", balance=" + balance + '.';
    }

    @Override
    public int compareTo(Object o) {
        BankAccount b = (BankAccount) o;
        if (this.balance > b.balance) return 1;
        if (this.balance == b.balance) return 0;
        return -1;
    }
}
