package robert.panta.lab6.ex3;

import robert.panta.lab6.ex1.BankAccount;

import java.util.Comparator;
import java.util.TreeSet;

public class Bank {
    public static TreeSet<BankAccount> accounts = new TreeSet<>();
    public static TreeSet<BankAccount> accounts2 = new TreeSet<>(new getAllAccounts());

    public void addAccount(String owner, double balance) {
        accounts.add(new BankAccount(owner, balance));
    }

    public void addAccount2(String owner, double balance) {
        accounts2.add(new BankAccount(owner, balance));
    }


    public void printAccounts() {
        for (BankAccount a : accounts)
            System.out.println(a + ",");
    }

    public void printAllAccounts() {
        for (BankAccount b : accounts2)
            System.out.println(b + ",");
    }

    public void printAccounts(double minBalance, double maxBalance) {
        System.out.println(accounts.subSet(new BankAccount("X", minBalance), new BankAccount("Y", maxBalance)));
    }


    static class getAllAccounts implements Comparator<BankAccount> {
        public int compare(BankAccount b21, BankAccount b22) {
            return b21.getOwner().compareTo(b22.getOwner());
        }
    }

}
