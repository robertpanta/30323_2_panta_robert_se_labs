package robert.panta.lab6.ex4;

import java.util.Objects;

public class Definition {
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Definition that = (Definition) o;
        return description.equals(that.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(description);
    }

    public final String description;

    public Definition(String description) {

        this.description = description;
    }

    public String toString() {

        return description;
    }
}

