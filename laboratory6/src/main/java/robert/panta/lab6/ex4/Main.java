package robert.panta.lab6.ex4;

import java.util.Scanner;

import static java.lang.System.in;

public class Main {
    public static void main(String[] args) {

        Dictionary dictionary = new Dictionary();
        int choice;
        Scanner scanner = new Scanner(in);

        do {
            System.out.println("1. Add new word" +
                            "\n2. Word definition" + "\n3. Get all words" +
                    "\n4. Get all definitions" + "\n5. Exit");
            choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    dictionary.addWord(new Word("go"), new Definition("moving verb"));
                    break;
                case 2:
                    dictionary.getDefinition(new Word("go"));
                    break;
                case 3:
                    dictionary.getAllWords();
                    break;
                case 4:
                    dictionary.getAllDefinition();
                    break;
            }
        } while (choice != 5);
    }
}