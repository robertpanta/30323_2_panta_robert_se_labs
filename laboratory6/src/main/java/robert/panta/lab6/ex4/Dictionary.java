package robert.panta.lab6.ex4;

import java.util.HashMap;

public class Dictionary {

    HashMap dictionary = new HashMap();

    public void addWord(Word w, Definition d) {
        if (dictionary.containsKey(w))
            System.out.println("Already Exists!");
        else
            System.out.println("New word added!");
        dictionary.put(w, d);
    }

    public String getDefinition(Word w) {
        System.out.println("Searching " + w);
        System.out.println(dictionary.containsKey(w));
        return dictionary.get(w).toString();
    }

    public void getAllWords() {
        for (Object word : dictionary.keySet()) {
            System.out.println(word.toString());
        }
    }

    public void getAllDefinition() {
        for (Object w : dictionary.keySet()) {
            System.out.println(dictionary.get(w).toString());
        }
    }
}
