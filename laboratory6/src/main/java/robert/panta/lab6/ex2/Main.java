package robert.panta.lab6.ex2;

public class Main {

    public static void main(String[] args) {
        Bank bank2 = new Bank();
        bank2.addAccount("Jack", 200);
        bank2.addAccount("Mary", 350);
        bank2.addAccount("Johnny", 100);
        bank2.addAccount("Robert", 160);
        bank2.addAccount("Joe", 440);
        System.out.println("\n The accounts sorted by name: ");
        bank2.printAccounts();

    }
}
