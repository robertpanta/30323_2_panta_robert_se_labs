package robert.panta.lab6.ex2;

import robert.panta.lab6.ex1.BankAccount;

import java.util.ArrayList;

public class Bank {

    ArrayList<BankAccount> accounts;

    public Bank() {
        accounts = new ArrayList<>();
    }

    public void addAccount(String owner, double balance) {
        accounts.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        int n = accounts.size();
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (accounts.get(j).getBalance() > accounts.get(j + 1).getBalance()) {
                    BankAccount temp = accounts.get(j);
                    accounts.set(j, accounts.get(j + 1));
                    accounts.set(j + 1, temp);
                }
        for (int i = 0; i < accounts.size(); i++) {
            System.out.println(accounts.get(i));
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (int i = 0; i < accounts.size(); i++) {
            if (accounts.get(i).getBalance() > minBalance && accounts.get(i).getBalance() < maxBalance) {
                System.out.println(accounts.get(i));
            }
        }
    }

    public ArrayList<BankAccount> getAllAccounts() {

        return accounts;
    }
}