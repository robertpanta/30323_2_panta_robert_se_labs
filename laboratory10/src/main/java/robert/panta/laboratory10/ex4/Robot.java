package robert.panta.laboratory10.ex4;

import java.util.ArrayList;

public class Robot {

    int x;
    int y;
    public static final int DOWN = 1;
    public static final int LEFT = 2;
    public static final int LEFT_DOWN_CORNER = 3;
    public static final int LEFT_UP_CORNER = 4;
    public static final int RIGHT = 5;
    public static final int RIGHT_DOWN_CORNER = 6;
    public static final int RIGHT_UP_CORNER = 7;
    public static final int UP = 8;

    //getters
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    //setters
    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    //constructor
    public Robot(int x, int y) {
        this.x = x;
        this.y = y;
    }
}

