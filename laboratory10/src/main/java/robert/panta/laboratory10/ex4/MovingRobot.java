package robert.panta.laboratory10.ex4;

import java.util.ArrayList;

public class MovingRobot extends Robot {
    ArrayList<Integer> moves;
    int nextMove;


    //constructor
    public MovingRobot(int x, int y) {
        super(x, y);
    }

    public boolean validateNextMove() {
        if ((nextMove == LEFT || nextMove == LEFT_UP_CORNER || nextMove == LEFT_DOWN_CORNER) && (this.getY() == 0))
            return false;
        if ((nextMove == RIGHT || nextMove == RIGHT_UP_CORNER || nextMove == RIGHT_DOWN_CORNER) && (this.getY() == 9))
            return false;
        if ((nextMove == UP || nextMove == LEFT_UP_CORNER || nextMove == RIGHT_UP_CORNER) && (this.getX() == 0))
            return false;
        if ((nextMove == DOWN || nextMove == LEFT_DOWN_CORNER || nextMove == RIGHT_DOWN_CORNER) && (this.getX() == 9))
            return false;
        else
            return true;
    }

    public int generateNextMove() {
        return (int) (Math.random() * 8) + 1;
    }

    public static boolean sameSlot(Robot r1, Robot r2) {
        if ((r1.getX() == r2.getX()) && (r1.getY() == r2.getY()))
            return true;
        else
            return false;
    }

    public String printMoves() {
        String movesReport = "";
        for (var nextMove : moves) {
            if (nextMove == DOWN) movesReport += "Down, ";
            if (nextMove == LEFT) movesReport += "Left, ";
            if (nextMove == LEFT_DOWN_CORNER) movesReport += "Left Down Corner, ";
            if (nextMove == LEFT_UP_CORNER) movesReport += "Left Up Corner, ";
            if (nextMove == RIGHT) movesReport += "Right, ";
            if (nextMove == RIGHT_DOWN_CORNER) movesReport += "Right Down Corner, ";
            if (nextMove == RIGHT_UP_CORNER) movesReport += "Right Up Corner, ";
            if (nextMove == UP) movesReport += "Up, ";
        }
        return movesReport;
    }

    public void move() {
        do nextMove = generateNextMove();
        while (validateNextMove() == false);
        switch (nextMove) {
            case 1: //LEFT
                this.x -= 1;
                moves.add(nextMove);
                break;
            case 2: //LEFT DOWN
                this.x -= 1;
                this.y += 1;
                moves.add(nextMove);
                break;
            case 3: //LEFT UP
                this.x -= 1;
                this.y -= 1;
                moves.add(nextMove);
                break;
            case 4: //RIGHT
                this.x += 1;
                moves.add(nextMove);
                break;
            case 5: // RIGHT DOWN
                this.x += 1;
                this.y += 1;
                moves.add(nextMove);
                break;
            case 6: // RIGHT UP
                this.x += 1;
                this.y -= 1;
                moves.add(nextMove);
                break;
            case 7: // UP
                this.y -= 1;
                moves.add(nextMove);
                break;
            case 8: //DOWN
                this.y += 1;
                moves.add(nextMove);
                break;
            default:
                System.out.print("invalid move");
        }
    }

    public static void main(String[] args) {
        var r1 = new MovingRobot(0, 0);
        var r2 = new MovingRobot(9, 9);
        r1.moves = new ArrayList<>();
        r2.moves = new ArrayList<>();
        while (!sameSlot(r1, r2)) {
            r1.move();
            if (!sameSlot(r1, r2)) {
                r2.move();
            }
        }
        System.out.println("Robot 1's moves were: " + r1.printMoves());
        System.out.println("Robot 2's moves were: " + r2.printMoves());
    }
}
