package robert.panta.laboratory10.ex3;

public class Counter extends Thread {

    Thread thread;
    String name;
    int value;

    Counter(String name, Thread thread, int value) {

        this.name = name;
        this.thread = thread;
        this.value = value;

    }

    public int getValue() {

        return this.value;

    }

    public void run() {

        int i;
        System.out.println("Counter " + name + " started to run!");
        try {
            if (thread != null) thread.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (i = this.value - 1; i <= this.value + 100; i++) {
            System.out.println(this.name + " i = " + i);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        this.value = i;
        System.out.println(this.name + " job is finalised.");

    }

    public static void main(String[] args) {

        Counter c1 = new Counter("counter1", null, 1);
        c1.run();

        Counter c2 = new Counter("counter2", c1, c1.getValue());
        c2.run();
    }
}
