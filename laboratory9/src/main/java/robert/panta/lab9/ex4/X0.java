package robert.panta.lab9.ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class X0 extends JFrame {

    JTextArea c1;
    JTextArea c2;
    JTextArea c3;
    JTextArea c4;
    JTextArea c5;
    JTextArea c6;
    JTextArea c7;
    JTextArea c8;
    JTextArea c9;
    JTextArea result;
    JButton buttoncheck;


    X0() {
        setTitle("X and 0");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 300);
        setVisible(true);

    }

    void init() {
        this.setLayout(null);
        int width = 30;
        int length = 30;

        c1 = new JTextArea();
        c1.setBounds(10, 10, width, length);

        c2 = new JTextArea();
        c2.setBounds(50, 10, width, length);

        c3 = new JTextArea();
        c3.setBounds(90, 10, width, length);

        c4 = new JTextArea();
        c4.setBounds(10, 50, width, length);

        c5 = new JTextArea();
        c5.setBounds(50, 50, width, length);

        c6 = new JTextArea();
        c6.setBounds(90, 50, width, length);

        c7 = new JTextArea();
        c7.setBounds(10, 90, width, length);

        c8 = new JTextArea();
        c8.setBounds(50, 90, width, length);

        c9 = new JTextArea();
        c9.setBounds(90, 90, width, length);

        result = new JTextArea();
        result.setBounds(10, 150, 110, 50);

        buttoncheck = new JButton("Check");
        buttoncheck.setBounds(170, 90, 70, 70);
        buttoncheck.addActionListener(new CheckButton());

        add(c1);
        add(c2);
        add(c3);
        add(c4);
        add(c5);
        add(c6);
        add(c7);
        add(c8);
        add(c9);
        add(result);
        add(buttoncheck);
    }

    public void checkCol() {

        //check the first column
        if (c1.getText().equals("X") && c4.getText().equals("X") && c7.getText().equals("X")) {
            result.setText("X wins");
        } else if (c1.getText().equals("0") && c4.getText().equals("0") && c7.getText().equals("0")) {
            result.setText("0 wins");
        }
        //check the second column
        else if (c2.getText().equals("X") && c5.getText().equals("X") && c8.getText().equals("X")) {
            result.setText("X wins");
        } else if (c2.getText().equals("0") && c5.getText().equals("0") && c8.getText().equals("0")) {
            result.setText("0 wins");
        }
        //check the third column
        else if (c3.getText().equals("X") && c6.getText().equals("X") && c9.getText().equals("X")) {
            result.setText("X wins");
        } else if (c3.getText().equals("0") && c6.getText().equals("0") && c9.getText().equals("0")) {
            result.setText("0 wins");
        }

    }

    public void checkRow() {

        //check the first row
        if (c1.getText().equals("X") && c2.getText().equals("X") && c3.getText().equals("X")) {
            result.setText("X wins");
        } else if (c1.getText().equals("0") && c2.getText().equals("0") && c3.getText().equals("0")) {
            result.setText("0 wins");
        }
        //check the second row
        else if (c4.getText().equals("X") && c5.getText().equals("X") && c6.getText().equals("X")) {
            result.setText("X wins");
        } else if (c4.getText().equals("0") && c5.getText().equals("0") && c6.getText().equals("0")) {
            result.setText("0 wins");
        }
        //check the third row
        else if (c7.getText().equals("X") && c8.getText().equals("X") && c9.getText().equals("X")) {
            result.setText("X wins");
        } else if (c7.getText().equals("0") && c8.getText().equals("0") && c9.getText().equals("0")) {
            result.setText("0 wins");
        }
    }

    public void checkDiag() {

        //check the first diagonal
        if (c1.getText().equals("X") && c5.getText().equals("X") && c9.getText().equals("X")) {
            result.setText("X wins");
        } else if (c1.getText().equals("0") && c5.getText().equals("0") && c9.getText().equals("0")) {
            result.setText("0 wins");
        }
        //check the second diagonal
        else if (c3.getText().equals("X") && c5.getText().equals("X") && c7.getText().equals("X")) {
            result.setText("X wins");
        } else if (c3.getText().equals("0") && c5.getText().equals("0") && c7.getText().equals("0")) {
            result.setText("0 wins");
        }
    }

    public static void main(String[] args) {
        new X0();
    }

    class CheckButton implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            checkCol();
            checkRow();
            checkDiag();
        }
    }
}
