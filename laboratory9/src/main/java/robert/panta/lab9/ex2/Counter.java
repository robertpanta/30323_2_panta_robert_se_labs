package robert.panta.lab9.ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter extends JFrame {

    JLabel counter;
    JTextArea result;
    JButton bCounter;
    int count = 0;

    public Counter() {
        setTitle("Counter");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 300);
        setVisible(true);
    }

    public void init() {

        this.setLayout(null);
        int width = 100;
        int heigth = 100;

        counter = new JLabel("Counter");
        counter.setBounds(125, 10, width, heigth);

        bCounter = new JButton("Press me!");
        bCounter.setBounds(100, 70, width, heigth);

        bCounter.addActionListener(new ButonCount());

        result = new JTextArea(count + "");
        result.setBounds(100, 180, 100, 40);
        result.setEditable(false);

        add(counter);
        add(bCounter);
        add(result);
    }

    public static void main(String[] args) {
        new Counter();
    }

    class ButonCount implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            ++count;
            result.setText(count + "");

        }
    }
}
