package robert.panta.lab9.ex3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;

class FileWrite extends JFrame {
    JButton bSave;
    JTextArea textArea;
    JTextArea textField;

    FileWrite() {
        setTitle("Write File");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(500, 500);
        setVisible(true);
    }

    void init() {

        this.setLayout(null);
        textField = new JTextArea();
        textField.setBounds(200, 10, 50, 30);

        textArea = new JTextArea();
        textArea.setBounds(200, 60, 50, 30);

        bSave = new JButton("Show");
        bSave.setBounds(200, 120, 70, 50);
        bSave.addActionListener(new SaveButon());

        add(textArea);
        add(bSave);
        add(textField);
    }

    public static void main(String[] args) {
        new FileWrite();
    }

    void open(String fileName) {

        try {
            textArea.setText("");
            BufferedReader bf = new BufferedReader(new FileReader(fileName));
            String l = "";
            l = bf.readLine();
            while (l != null) {
                l = bf.readLine();
                textArea.append(l + "\n");
            }
        } catch (Exception e) {
        }
    }

    class SaveButon implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String fileName = textField.getText();
            textArea.setText(fileName);
        }
    }
}
