package ex1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRobot {

    @Test
    void testConstructor() {
        Robot robot = new Robot();
        assertEquals(1, robot.getX());
    }

    @Test
    void testChangeWhenValueIsHigherThanOne() {
        Robot robot = new Robot();
        assertEquals(1, robot.getX());
        robot.change(5);
        assertEquals(6, robot.getX());
    }

    @Test
    void testChangeWhenValueIsLowerThanOne() {
        Robot robot = new Robot();
        assertEquals(1, robot.getX());
        robot.change(0);
        assertEquals(1, robot.getX());
    }
}