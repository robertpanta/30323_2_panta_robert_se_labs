package ex2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCircle {

    @Test
    void testConstructorDefault() {
        Circle circle = new Circle();
        assertEquals(3.14, circle.getArea(), 0.01);
        assertEquals(1, circle.getRadius());
    }

    @Test
    void testConstructor() {
        Circle circle = new Circle(1);
        assertEquals(3.14, circle.getArea(), 0.01);
        assertEquals(1, circle.getRadius());
    }
}


