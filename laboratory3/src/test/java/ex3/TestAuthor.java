package ex3;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAuthor {

    @Test
    void testConstructor() {
        Author author = new Author("Robert", "robert_v.panta@yahoo.com", 'm');
        assertEquals("Robert", author.getName());
        assertEquals("robert_v.panta@yahoo.com", author.getEmail());
        assertEquals('m', author.getGender());
    }

    @Test
    void testSetEmail() {
        Author author = new Author("Robert", "robert_v.panta@yahoo.com", 'm');
        author.setEmail("johnny@gmail.com");
        assertEquals("johnny@gmail.com", author.getEmail());
    }

    @Test
    void testToString() {
        Author author = new Author("Robert", "robert_v.panta@yahoo.com", 'm');
        assertEquals("Robert (m) at robert_v.panta@yahoo.com", author.toString());
    }
}


