package ex4;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMyPoint {


    @Test
    void testConstructorDefault() {
        MyPoint point = new MyPoint();
        assertEquals(0, point.getX(), point.getY());
    }

    @Test
    void testGetX() {
        MyPoint point = new MyPoint();
        assertEquals(0, point.getX());
    }

    @Test
    void testGetY() {
        MyPoint point = new MyPoint();
        assertEquals(0, point.getY());
    }

    @Test
    void testSetX() {
        MyPoint point = new MyPoint();
        point.setX(2);
        assertEquals(2, point.getX());
    }

    @Test
    void testSetY() {
        MyPoint point = new MyPoint();
        point.setY(4);
        assertEquals(4, point.getY());
    }

    @Test
    void testSetXY() {
        MyPoint point = new MyPoint();
        point.setXY(2, 4);
        assertEquals(4, point.getX(), point.getY());
    }

    @Test
    void testGetDistanceDefault() {
        MyPoint point = new MyPoint();
        assertEquals(10.0, point.getDistance(6, 8));
    }

    @Test
    void testGetDistance() {
        MyPoint point = new MyPoint(2, 7);
        assertEquals(7.280109889280518, point.getDistance(0, 0));
    }

    @Test
    void testGetDistanceBetweenTwoPoints() {
        MyPoint point1 = new MyPoint();
        MyPoint point2 = new MyPoint(2, 7);
        assertEquals(7.28, point1.getDistance(point2),0.01);
    }
}
