package ex2;

public class Circle {
    private String color;
    private double radius;

    public Circle() { //default contructor
        color = "red";
        radius = 1.0;
    }

    public Circle(double rad) {
        color = "red";
        radius = rad;
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        return Math.PI * (radius * radius);
    }

}
