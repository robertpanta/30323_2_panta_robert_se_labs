package ex5;

public class Flower {
    static int counts = 0;

    Flower() {
        counts++;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[10];

        for (int i = 0; i < 10; i++) {
            Flower flower = new Flower();
            garden[i] = flower;
        }
        System.out.println("There are " + counts + " created objects.");
    }
}
