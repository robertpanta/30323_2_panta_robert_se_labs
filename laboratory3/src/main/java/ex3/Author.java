package ex3;

public class Author {
    private final String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender) {
        //constructor used to initialize
        this.name = name;
        this.email = email;
        if (gender == 'm' || gender == 'f') {
            this.gender = gender;
        }
    }

    public String getName() {
        //public getter
        return name;
    }

    public String getEmail() {
        //public getter
        return email;
    }

    public char getGender() {
        //public getter
        return gender;
    }

    public void setEmail(String email) {
        //public setter -> the only one attribute that can be changed
        this.email = email;
    }

    @Override
    public String toString() {
        return this.name + " (" + this.gender + ") at " + this.email;
    }

}
