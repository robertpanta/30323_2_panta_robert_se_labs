package ex1;

public class Circle {
    public double radius;
    private String color;

    public Circle() {
        radius = 1.0;
        color = "red";
    }

    public Circle(double rad) { //constructor
        radius = rad;
        color = "red";
    }

    public double getRadius() {
        return radius;
    }


    public double getArea() {
        return Math.PI * radius * radius;
    }

}