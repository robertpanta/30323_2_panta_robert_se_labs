package ex5;

import ex1.Circle;

public class Cylinder extends Circle {
    private double height;

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder() {
        super();
        height = 1.0;
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return Math.PI * getRadius() * getRadius() * height;
    }

    @Override
    public String toString() {
        return "Cylinder{" + "heights: " + height + "}";
    }

    @Override
    public double getArea() {
        return 2 * Math.PI * radius * height + 2 * Math.PI * radius * radius;
    }
}