package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestShape {
    @Test
    public void testConstructor() {
        Shape shape1 = new Shape("red", true);
        assertEquals("red", shape1.getColor());
        assertEquals(true, shape1.isFilled());
    }

    @Test
    public void testGetColor() {
        Shape shape1 = new Shape("red", true);
        assertEquals("red", shape1.getColor());
    }

    @Test
    public void testGetFilled() {
        Shape shape1 = new Shape("red", true);
        assertEquals(true, shape1.isFilled());
    }

    @Test
    public void testSetColor() {
        Shape shape1 = new Shape("red", true);
        shape1.setColor("blue");
        assertEquals("blue", shape1.getColor());
    }

    @Test
    public void testSetFilled() {
        Shape shape1 = new Shape("red", true);
        shape1.setFilled(false);
        assertEquals(false, shape1.isFilled());
    }

    @Test
    public void testToString() {
        Shape shape1 = new Shape("red", true);
        assertEquals("A Shape with colour of " + shape1.getColor() + " and filled", shape1.toString());
    }
}