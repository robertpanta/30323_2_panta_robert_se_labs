package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestSquare {
    @Test
    public void testConstructor() {
        Square square1 = new Square(1);
        assertEquals(1, square1.getSide());
    }

    @Test
    public void testConstructor2() {
        Square square1 = new Square();
        assertEquals(1.0, square1.getSide());
    }

    @Test
    public void testConstructor1() {
        Square square1 = new Square(1, "red", true);
        assertEquals(1, square1.getSide());
        assertEquals("red", square1.getColor());
        assertEquals(true, square1.isFilled());
    }

    @Test
    public void testGetSide() {
        Square square1 = new Square(1);
        assertEquals(1.0, square1.getSide());
    }

    @Test
    public void testSetSide() {
        Square square1 = new Square(1);
        square1.setSide(7);
        assertEquals(7, square1.getSide());
    }

    @Test
    public void testSetWidth() {
        Square square1 = new Square(1);
        square1.setWidth(2);
        assertEquals(2, square1.getWidth());
    }

    @Test
    public void testSetLength() {
        Square square1 = new Square(1);
        square1.setLength(4);
        assertEquals(4, square1.getLength());
    }

    @Test
    public void testToString() {
        Square square1 = new Square(1);
        Rectangle rectangle1 = new Rectangle();
        assertEquals("A Square with side= " + square1.getSide() + " which is a subclass of " + rectangle1.toString(), square1.toString());
    }
}