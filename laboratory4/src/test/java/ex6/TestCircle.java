package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCircle extends Shape {
    @Test
    public void testConstructor() {
        Circle circle1 = new Circle();
        assertEquals(1, circle1.getRadius());
    }

    @Test
    public void testConstructor1() {
        Circle circle1 = new Circle(1);
        assertEquals(1, circle1.getRadius());
        assertEquals("yellow", circle1.getColor());
    }

    @Test
    public void testConstructor2() {
        Circle circle1 = new Circle(1, "yellow", true);
        assertEquals(1, circle1.getRadius());
        assertEquals("yellow", circle1.getColor());
        assertEquals(true, circle1.isFilled());
    }

    @Test
    public void testGetRadius() {
        Circle circle1 = new Circle(2);
        assertEquals(2, circle1.getRadius());
    }

    @Test
    public void testSetRadius() {
        Circle circle1 = new Circle(2);
        circle1.setRadius(3);
        assertEquals(3, circle1.getRadius());
    }

    @Test
    public void testGetColor() {
        Circle circle1 = new Circle(2);
        assertEquals("yellow", circle1.getColor());
    }

    @Test
    public void testGetPerimeter() {
        Circle circle1 = new Circle(2);
        assertEquals(2 * circle1.getRadius() * Math.PI, circle1.getPerimeter());
    }

    @Test
    public void testGetArea() {
        Circle circle1 = new Circle(2);
        assertEquals(2 * circle1.getRadius() * Math.PI, circle1.getArea());
    }
}