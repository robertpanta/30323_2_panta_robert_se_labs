package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRectangle {
    @Test
    public void testConstructor1() {
        Rectangle rectangle1 = new Rectangle(2, 4);
        assertEquals(rectangle1.width, rectangle1.getWidth());
        assertEquals(rectangle1.length, rectangle1.getLength());
    }

    @Test
    public void testConstructor2() {
        Rectangle rectangle1 = new Rectangle(1, 2, "red", true);
        assertEquals(rectangle1.width, rectangle1.getWidth());
        assertEquals(rectangle1.length, rectangle1.getLength());
        assertEquals(rectangle1.color, rectangle1.getColor());
        assertEquals(true, rectangle1.isFilled());
    }

    @Test
    public void testSetWidth() {
        Rectangle rectangle1 = new Rectangle(1, 2);
        rectangle1.setWidth(1);
        assertEquals(rectangle1.width, rectangle1.getWidth());
    }

    @Test
    public void testSetLength() {
        Rectangle rectangle1 = new Rectangle(1, 2);
        rectangle1.setLength(4);
        assertEquals(rectangle1.length, rectangle1.getLength());
    }

    @Test
    public void testGetArea() {
        Rectangle rectangle1 = new Rectangle(1, 2);
        assertEquals(rectangle1.length * rectangle1.width, rectangle1.getArea());
    }

    @Test
    public void testGetPerimeter() {
        Rectangle rectangle1 = new Rectangle(1, 2);
        assertEquals((rectangle1.length + rectangle1.width) * 2, rectangle1.getPerimeter());
    }

    @Test
    public void testToString() {
        Rectangle rectangle1 = new Rectangle(1, 2);
        Shape shape1 = new Shape();
        assertEquals("A Rectangle with width = " + rectangle1.width + " and length = " + rectangle1.length + " which is a subclass of " + shape1.toString(), rectangle1.toString());
    }
}