package ex1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCircle {
    @Test
    public void testConstructor1() {
        Circle circle1 = new Circle();
        assertEquals(1, circle1.getRadius());
        assertEquals(Math.PI, circle1.getArea());
    }

    @Test
    public void testGetRadius() {
        Circle circle1 = new Circle();
        assertEquals(1, circle1.getRadius());
    }

    @Test
    public void testGetArea() {
        Circle circle1 = new Circle();
        assertEquals(Math.PI, circle1.getArea());
    }
}