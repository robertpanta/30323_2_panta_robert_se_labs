package ex3;

import ex2.Author;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestBook {
    @Test
    public void testConstructor1() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        Book book1 = new Book("TheBestBookEver", author1, 50);
        assertEquals("TheBestBookEver", book1.getName());
        assertEquals("Robert DJunior", book1.getAuthor().getName());
        assertEquals(50, book1.getPrice());
    }

    @Test
    public void testConstructor2() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        Book book1 = new Book("TheBestBookEver", author1, 50, 0);
        assertEquals("TheBestBookEver", book1.getName());
        assertEquals("Robert DJunior", book1.getAuthor().getName());
        assertEquals(50, book1.getPrice());
        assertEquals(0, book1.getQtyInStock());
    }

    @Test
    public void testGetName() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        Book book1 = new Book("TheBestBookEver", author1, 50);
        assertEquals("TheBestBookEver", book1.getName());
    }

    @Test
    public void testGetAuthor() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        Book book1 = new Book("TheBestBookEver", author1, 50);
        assertEquals("Robert DJunior", book1.getAuthor().getName());
    }

    @Test
    public void testGetPrice() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        Book book1 = new Book("TheBestBookEver", author1, 50);
        assertEquals(50, book1.getPrice());
    }

    @Test
    public void testGetQtyInStock() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        Book book1 = new Book("TheBestBookEver", author1, 50, 0);
        assertEquals(0, book1.getQtyInStock());
    }

    @Test
    public void testSetPrice() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        Book book1 = new Book("TheBestBookEver", author1, 50);
        book1.setPrice(59.99);
        assertEquals(59.99, book1.getPrice());
    }

    @Test
    public void testSetQtyInStock() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        Book book1 = new Book("TheBestBookEver", author1, 50, 0);
        book1.setQtyInStock(100);
        assertEquals(100, book1.getQtyInStock());
    }

    @Test
    public void testToString() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        Book book1 = new Book("TheBestBookEver", author1, 50);
        assertEquals("book - " + book1.getName() + " by author " + book1.getAuthor() + "(" + book1.getAuthor().getGender() + ") at " + book1.getAuthor().getEmail(), book1.toString());
    }
}
