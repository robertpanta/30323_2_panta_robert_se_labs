package ex2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAuthor {

    @Test
    public void testConstructor() {
        Author author = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        assertEquals("Robert DJunior", author.getName());
        assertEquals("robertdj@yahoo.com", author.getEmail());
        assertEquals('m', author.getGender());
    }

    @Test
    public void testGetName() {
        Author author = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        assertEquals("Robert DJunior", author.getName());
    }

    @Test
    public void testGetGender() {
        Author author = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        assertEquals('m', author.getGender());
    }

    @Test
    public void testGetEmail() {
        Author author = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        assertEquals("robertdj@yahoo.com", author.getEmail());
    }

    @Test
    public void testSetEmail() {
        Author author = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        author.setEmail("RobertDJ@yahoo.com");
        assertEquals("RobertDJ@yahoo.com", author.getEmail());
    }

    @Test
    public void testToString() {
        Author author = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        assertEquals("Robert DJunior (m) at robertdj@yahoo.com", author.toString());

    }
}
