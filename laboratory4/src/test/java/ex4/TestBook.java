package ex4;

import ex2.Author;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestBook {
    @Test
    public void testConstructor1() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        ex3.Book book1 = new ex3.Book("TheBestBookEver", author1, 50);
        assertEquals("TheBestBookEver", book1.getName());
        assertEquals("Robert DJunior", book1.getAuthor().getName());
        assertEquals(50, book1.getPrice());
    }


    @Test
    public void testConstructor2() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        ex3.Book book1 = new ex3.Book("TheBestBookEver", author1, 50, 0);
        assertEquals("TheBestBookEver", book1.getName());
        assertEquals("Robert DJunior", book1.getAuthor().getName());
        assertEquals(50, book1.getPrice());
        assertEquals(0, book1.getQtyInStock());
    }

    @Test
    public void testGetName() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        ex3.Book book1 = new ex3.Book("TheBestBookEver", author1, 50);
        assertEquals("TheBestBookEver", book1.getName());
    }

    @Test
    public void testGetAuthor() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        ex3.Book book1 = new ex3.Book("TheBestBookEver", author1, 50);
        assertEquals("Robert DJunior", book1.getAuthor().getName());
    }

    @Test
    public void testGetPrice() {
        Author author1 = new Author("BRobert DJunior", "robertdj@yahoo.com", 'm');
        ex3.Book book1 = new ex3.Book("TheBestBookEver", author1, 50);
        assertEquals(50, book1.getPrice());
    }

    @Test
    public void testGetQtyInStock() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        ex3.Book book1 = new ex3.Book("TheBestBookEver", author1, 50, 0);
        assertEquals(0, book1.getQtyInStock());
    }

    @Test
    public void testSetPrice() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        ex3.Book book1 = new ex3.Book("TheBestBookEver", author1, 50);
        book1.setPrice(59.99);
        assertEquals(59.99, book1.getPrice());
    }

    @Test
    public void testSetQtyInStock() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        ex3.Book book1 = new ex3.Book("TheBestBookEver", author1, 50, 0);
        book1.setQtyInStock(100);
        assertEquals(100, book1.getQtyInStock());
    }


    @Test
    public void testToString() {
        Author author1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        ex3.Book book1 = new ex3.Book("TheBestBookEver", author1, 50);
        assertEquals("book - " + book1.getName() + " by " + "3" + " authors.", book1.toStringEx4());
    }


    @Test
    public void testPrintAuthors() {
        Author authors1 = new Author("Robert DJunior", "robertdj@yahoo.com", 'm');
        Author authors2 = new Author("BRobert DSenior", "robertds@yahoo.com", 'm');
        Author authors3 = new Author("Robert Junior", "robertj@yahoo.com", 'm');

        Author[] authors = new Author[]{authors1, authors2, authors3};

        Book book1 = new Book("TheBestBookEver", authors, 50, 0);
        book1.printAuthors();
    }
}
