package ex5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCylinder {
    @Test
    public void testConstruct() {
        Cylinder cylinder1 = new Cylinder();
        assertEquals(1, cylinder1.getRadius());
    }

    @Test
    public void testConstruct1() {
        Cylinder cylinder1 = new Cylinder(1);
        assertEquals(1, cylinder1.getRadius());
    }

    @Test
    public void testConstruct2() {
        Cylinder cylinder1 = new Cylinder(1, 2);
        assertEquals(1, cylinder1.getRadius());
        assertEquals(2, cylinder1.getHeight());
    }

    @Test
    public void testGetRadius() {
        Cylinder cylinder1 = new Cylinder(1, 2);
        assertEquals(1, cylinder1.getRadius());
    }

    @Test
    public void testGetHeight() {
        Cylinder cylinder1 = new Cylinder(1, 2);
        assertEquals(2, cylinder1.getHeight());
    }

    @Test
    public void testGetVolume() {
        Cylinder cylinder1 = new Cylinder(1, 2);
        assertEquals(6.283185307179586, cylinder1.getVolume());
    }

    @Test
    public void testGetArea() {
        Cylinder cylinder1 = new Cylinder(1, 2);
        cylinder1.getArea();
        assertEquals(2 * Math.PI * cylinder1.getRadius() * cylinder1.getHeight() + 2 * Math.PI * cylinder1.getRadius() * cylinder1.getRadius(), cylinder1.getArea());
    }
}