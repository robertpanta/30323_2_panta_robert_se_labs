package robert.panta.laboratory11.ex1;

import javax.swing.*;
import java.awt.*;

public class UserInterface extends JFrame implements Observer {
    private JTextArea textArea = new JTextArea();
    private JPanel panel;
    private JFrame frame = new JFrame("Temperature sensor");

    @Override
    public void update(Object event) {
        textArea.append("Temperature: " + event.toString() + " °C" + "\n");
    }

    public UserInterface() {
        frame.setLayout(new GridLayout(1, 1));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 600);
        frame.add(textArea);
        frame.setVisible(true);

    }

    public static void main(String[] args) {
        Sensor tempSensor = new Sensor();
        UserInterface userint = new UserInterface();
        tempSensor.register(userint);
        Thread tr = new Thread(tempSensor);
        tr.start();
    }
}
