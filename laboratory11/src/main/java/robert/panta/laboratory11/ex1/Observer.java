package robert.panta.laboratory11.ex1;

interface Observer {
    public abstract void update(Object event);
}