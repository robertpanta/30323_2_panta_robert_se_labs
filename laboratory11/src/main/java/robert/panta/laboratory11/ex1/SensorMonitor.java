package robert.panta.laboratory11.ex1;

public class SensorMonitor extends Observable{
    void startSensor(){
        System.out.println("Sensor has been started!");
        this.changeState("START");
    }

    void stopSensor(){
        System.out.print("Sensor has been stopped!");
        this.changeState("STOP");
    }
}
