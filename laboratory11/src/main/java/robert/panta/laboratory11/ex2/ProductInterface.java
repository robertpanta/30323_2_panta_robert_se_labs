package robert.panta.laboratory11.ex2;

import javax.swing.*;

public class ProductInterface {
    private JPanel mainPanel;
    private JLabel productTitle;
    private JTextField productName;
    private JTextField productQuantity;
    private JTextField productPrice;
    private JButton addNewProductButton;
    private JButton viewAvailableProductButton;
    private JList<Product> productList;
    private JButton deleteSelectedProductButton;
    private JButton updateQuantityButton;
    private JTextField newQuantity;

    public JPanel getMainPanel() {
        return mainPanel;
    }

    public void setMainPanel(JPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    public JLabel getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(JLabel productTitle) {
        this.productTitle = productTitle;
    }

    public JTextField getProductName() {
        return productName;
    }

    public void setProductName(JTextField productName) {
        this.productName = productName;
    }

    public JTextField getProductQuantity() {
        return productQuantity;
    }

    public void setProductQuantity(JTextField productQuantity) {
        this.productQuantity = productQuantity;
    }

    public JTextField getProductPrice() {
        return productPrice;
    }

    public void setProductPrice(JTextField productPrice) {
        this.productPrice = productPrice;
    }

    public JButton getAddNewProductButton() {
        return addNewProductButton;
    }

    public void setAddNewProductButton(JButton addNewProductButton) {
        this.addNewProductButton = addNewProductButton;
    }

    public JButton getViewAvailableProductsButton() {
        return viewAvailableProductButton;
    }

    public void setViewAvailableProductButton(JButton viewAvailableProductButton) {
        this.viewAvailableProductButton = viewAvailableProductButton;
    }

    public JList getProductList() {
        return productList;
    }

    public void setProductList(JList<Product> productList) {
        this.productList = productList;
    }

    public JButton getDeleteSelectedProductButton() {
        return deleteSelectedProductButton;
    }

    public void setDeleteSelectedProductButton(JButton deleteSelectedProductButton) {
        this.deleteSelectedProductButton = deleteSelectedProductButton;
    }

    public JButton getUpdateQuantityButton() {
        return updateQuantityButton;
    }

    public void setUpdateQuantityButton(JButton updateQuantityButton) {
        this.updateQuantityButton = updateQuantityButton;
    }

    public JTextField getNewQuantity() {
        return newQuantity;
    }

    public void setNewQuantity(JTextField newQuantity) {
        this.newQuantity = newQuantity;
    }
}
