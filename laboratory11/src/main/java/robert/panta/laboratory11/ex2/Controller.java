package robert.panta.laboratory11.ex2;

import javax.swing.*;
import java.util.Vector;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Controller{
    private static final Vector<Product> products= new Vector<>(0);


    public static void main(String[] args) {
        JFrame frame = new JFrame("ProductInterface");
        var productInterface= new ProductInterface();
        productInterface.getAddNewProductButton().addActionListener(e -> {
            String productName = productInterface.getProductName().getText();
            Integer productOty = Integer.parseInt(productInterface.getProductQuantity().getText());
            Double productPrice = Double.parseDouble(productInterface.getProductPrice().getText());

            products.add(new Product(productName, productOty, productPrice));

        });

        productInterface.getViewAvailableProductsButton().addActionListener(e -> productInterface.getProductList().setListData(products));
        productInterface.getDeleteSelectedProductButton().addActionListener(e -> products.remove(productInterface.getProductList().getSelectedValuesList().remove(products)));
        productInterface.getUpdateQuantityButton().addActionListener(e -> products.get(productInterface.getProductList().getSelectedValue().getClass().getModifiers()));

        frame.setContentPane(productInterface.getMainPanel());
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
